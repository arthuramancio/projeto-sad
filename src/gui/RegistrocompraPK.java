/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Arthur
 */
@Embeddable
public class RegistrocompraPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "registrocompraId")
    private int registrocompraId;
    @Basic(optional = false)
    @Column(name = "Usuario_usuarioId")
    private int usuariousuarioId;

    public RegistrocompraPK() {
    }

    public RegistrocompraPK(int registrocompraId, int usuariousuarioId) {
        this.registrocompraId = registrocompraId;
        this.usuariousuarioId = usuariousuarioId;
    }

    public int getRegistrocompraId() {
        return registrocompraId;
    }

    public void setRegistrocompraId(int registrocompraId) {
        this.registrocompraId = registrocompraId;
    }

    public int getUsuariousuarioId() {
        return usuariousuarioId;
    }

    public void setUsuariousuarioId(int usuariousuarioId) {
        this.usuariousuarioId = usuariousuarioId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) registrocompraId;
        hash += (int) usuariousuarioId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegistrocompraPK)) {
            return false;
        }
        RegistrocompraPK other = (RegistrocompraPK) object;
        if (this.registrocompraId != other.registrocompraId) {
            return false;
        }
        if (this.usuariousuarioId != other.usuariousuarioId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gui.RegistrocompraPK[ registrocompraId=" + registrocompraId + ", usuariousuarioId=" + usuariousuarioId + " ]";
    }
    
}
