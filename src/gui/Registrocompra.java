/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Arthur
 */
@Entity
@Table(name = "registrocompra", catalog = "dbsad", schema = "")
@NamedQueries({
    @NamedQuery(name = "Registrocompra.findAll", query = "SELECT r FROM Registrocompra r")
    , @NamedQuery(name = "Registrocompra.findByRegistrocompraId", query = "SELECT r FROM Registrocompra r WHERE r.registrocompraPK.registrocompraId = :registrocompraId")
    , @NamedQuery(name = "Registrocompra.findByRegistrocompraData", query = "SELECT r FROM Registrocompra r WHERE r.registrocompraData = :registrocompraData")
    , @NamedQuery(name = "Registrocompra.findByUsuariousuarioId", query = "SELECT r FROM Registrocompra r WHERE r.registrocompraPK.usuariousuarioId = :usuariousuarioId")})
public class Registrocompra implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RegistrocompraPK registrocompraPK;
    @Basic(optional = false)
    @Column(name = "registrocompraData")
    private String registrocompraData;

    public Registrocompra() {
    }

    public Registrocompra(RegistrocompraPK registrocompraPK) {
        this.registrocompraPK = registrocompraPK;
    }

    public Registrocompra(RegistrocompraPK registrocompraPK, String registrocompraData) {
        this.registrocompraPK = registrocompraPK;
        this.registrocompraData = registrocompraData;
    }

    public Registrocompra(int registrocompraId, int usuariousuarioId) {
        this.registrocompraPK = new RegistrocompraPK(registrocompraId, usuariousuarioId);
    }

    public RegistrocompraPK getRegistrocompraPK() {
        return registrocompraPK;
    }

    public void setRegistrocompraPK(RegistrocompraPK registrocompraPK) {
        this.registrocompraPK = registrocompraPK;
    }

    public String getRegistrocompraData() {
        return registrocompraData;
    }

    public void setRegistrocompraData(String registrocompraData) {
        String oldRegistrocompraData = this.registrocompraData;
        this.registrocompraData = registrocompraData;
        changeSupport.firePropertyChange("registrocompraData", oldRegistrocompraData, registrocompraData);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (registrocompraPK != null ? registrocompraPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Registrocompra)) {
            return false;
        }
        Registrocompra other = (Registrocompra) object;
        if ((this.registrocompraPK == null && other.registrocompraPK != null) || (this.registrocompraPK != null && !this.registrocompraPK.equals(other.registrocompraPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gui.Registrocompra[ registrocompraPK=" + registrocompraPK + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
