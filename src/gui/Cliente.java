/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Arthur
 */
@Entity
@Table(name = "cliente", catalog = "dbsad", schema = "")
@NamedQueries({
    @NamedQuery(name = "Cliente.findAll", query = "SELECT c FROM Cliente c")
    , @NamedQuery(name = "Cliente.findByClienteIdcliente", query = "SELECT c FROM Cliente c WHERE c.clienteIdcliente = :clienteIdcliente")
    , @NamedQuery(name = "Cliente.findByClienteNome", query = "SELECT c FROM Cliente c WHERE c.clienteNome = :clienteNome")
    , @NamedQuery(name = "Cliente.findByClienteCpf", query = "SELECT c FROM Cliente c WHERE c.clienteCpf = :clienteCpf")
    , @NamedQuery(name = "Cliente.findByClienteEndereco", query = "SELECT c FROM Cliente c WHERE c.clienteEndereco = :clienteEndereco")
    , @NamedQuery(name = "Cliente.findByClienteTelefone", query = "SELECT c FROM Cliente c WHERE c.clienteTelefone = :clienteTelefone")})
public class Cliente implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "clienteIdcliente")
    private Integer clienteIdcliente;
    @Basic(optional = false)
    @Column(name = "clienteNome")
    private String clienteNome;
    @Basic(optional = false)
    @Column(name = "clienteCpf")
    private String clienteCpf;
    @Basic(optional = false)
    @Column(name = "clienteEndereco")
    private String clienteEndereco;
    @Basic(optional = false)
    @Column(name = "clienteTelefone")
    private String clienteTelefone;

    public Cliente() {
    }

    public Cliente(Integer clienteIdcliente) {
        this.clienteIdcliente = clienteIdcliente;
    }

    public Cliente(Integer clienteIdcliente, String clienteNome, String clienteCpf, String clienteEndereco, String clienteTelefone) {
        this.clienteIdcliente = clienteIdcliente;
        this.clienteNome = clienteNome;
        this.clienteCpf = clienteCpf;
        this.clienteEndereco = clienteEndereco;
        this.clienteTelefone = clienteTelefone;
    }

    public Integer getClienteIdcliente() {
        return clienteIdcliente;
    }

    public void setClienteIdcliente(Integer clienteIdcliente) {
        Integer oldClienteIdcliente = this.clienteIdcliente;
        this.clienteIdcliente = clienteIdcliente;
        changeSupport.firePropertyChange("clienteIdcliente", oldClienteIdcliente, clienteIdcliente);
    }

    public String getClienteNome() {
        return clienteNome;
    }

    public void setClienteNome(String clienteNome) {
        String oldClienteNome = this.clienteNome;
        this.clienteNome = clienteNome;
        changeSupport.firePropertyChange("clienteNome", oldClienteNome, clienteNome);
    }

    public String getClienteCpf() {
        return clienteCpf;
    }

    public void setClienteCpf(String clienteCpf) {
        String oldClienteCpf = this.clienteCpf;
        this.clienteCpf = clienteCpf;
        changeSupport.firePropertyChange("clienteCpf", oldClienteCpf, clienteCpf);
    }

    public String getClienteEndereco() {
        return clienteEndereco;
    }

    public void setClienteEndereco(String clienteEndereco) {
        String oldClienteEndereco = this.clienteEndereco;
        this.clienteEndereco = clienteEndereco;
        changeSupport.firePropertyChange("clienteEndereco", oldClienteEndereco, clienteEndereco);
    }

    public String getClienteTelefone() {
        return clienteTelefone;
    }

    public void setClienteTelefone(String clienteTelefone) {
        String oldClienteTelefone = this.clienteTelefone;
        this.clienteTelefone = clienteTelefone;
        changeSupport.firePropertyChange("clienteTelefone", oldClienteTelefone, clienteTelefone);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clienteIdcliente != null ? clienteIdcliente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cliente)) {
            return false;
        }
        Cliente other = (Cliente) object;
        if ((this.clienteIdcliente == null && other.clienteIdcliente != null) || (this.clienteIdcliente != null && !this.clienteIdcliente.equals(other.clienteIdcliente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gui.Cliente[ clienteIdcliente=" + clienteIdcliente + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
