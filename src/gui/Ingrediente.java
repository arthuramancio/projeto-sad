/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Arthur
 */
@Entity
@Table(name = "ingrediente", catalog = "dbsad", schema = "")
@NamedQueries({
    @NamedQuery(name = "Ingrediente.findAll", query = "SELECT i FROM Ingrediente i")
    , @NamedQuery(name = "Ingrediente.findByIngredienteId", query = "SELECT i FROM Ingrediente i WHERE i.ingredienteId = :ingredienteId")
    , @NamedQuery(name = "Ingrediente.findByIngredienteNome", query = "SELECT i FROM Ingrediente i WHERE i.ingredienteNome = :ingredienteNome")
    , @NamedQuery(name = "Ingrediente.findByIngredientePreco", query = "SELECT i FROM Ingrediente i WHERE i.ingredientePreco = :ingredientePreco")
    , @NamedQuery(name = "Ingrediente.findByIngredienteQuantidade", query = "SELECT i FROM Ingrediente i WHERE i.ingredienteQuantidade = :ingredienteQuantidade")
    , @NamedQuery(name = "Ingrediente.findByIngredienteDatacompra", query = "SELECT i FROM Ingrediente i WHERE i.ingredienteDatacompra = :ingredienteDatacompra")})
public class Ingrediente implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ingredienteId")
    private Integer ingredienteId;
    @Basic(optional = false)
    @Column(name = "ingredienteNome")
    private String ingredienteNome;
    @Basic(optional = false)
    @Column(name = "ingredientePreco")
    private String ingredientePreco;
    @Basic(optional = false)
    @Column(name = "ingredienteQuantidade")
    private String ingredienteQuantidade;
    @Basic(optional = false)
    @Column(name = "ingredienteDatacompra")
    private String ingredienteDatacompra;

    public Ingrediente() {
    }

    public Ingrediente(Integer ingredienteId) {
        this.ingredienteId = ingredienteId;
    }

    public Ingrediente(Integer ingredienteId, String ingredienteNome, String ingredientePreco, String ingredienteQuantidade, String ingredienteDatacompra) {
        this.ingredienteId = ingredienteId;
        this.ingredienteNome = ingredienteNome;
        this.ingredientePreco = ingredientePreco;
        this.ingredienteQuantidade = ingredienteQuantidade;
        this.ingredienteDatacompra = ingredienteDatacompra;
    }

    public Integer getIngredienteId() {
        return ingredienteId;
    }

    public void setIngredienteId(Integer ingredienteId) {
        Integer oldIngredienteId = this.ingredienteId;
        this.ingredienteId = ingredienteId;
        changeSupport.firePropertyChange("ingredienteId", oldIngredienteId, ingredienteId);
    }

    public String getIngredienteNome() {
        return ingredienteNome;
    }

    public void setIngredienteNome(String ingredienteNome) {
        String oldIngredienteNome = this.ingredienteNome;
        this.ingredienteNome = ingredienteNome;
        changeSupport.firePropertyChange("ingredienteNome", oldIngredienteNome, ingredienteNome);
    }

    public String getIngredientePreco() {
        return ingredientePreco;
    }

    public void setIngredientePreco(String ingredientePreco) {
        String oldIngredientePreco = this.ingredientePreco;
        this.ingredientePreco = ingredientePreco;
        changeSupport.firePropertyChange("ingredientePreco", oldIngredientePreco, ingredientePreco);
    }

    public String getIngredienteQuantidade() {
        return ingredienteQuantidade;
    }

    public void setIngredienteQuantidade(String ingredienteQuantidade) {
        String oldIngredienteQuantidade = this.ingredienteQuantidade;
        this.ingredienteQuantidade = ingredienteQuantidade;
        changeSupport.firePropertyChange("ingredienteQuantidade", oldIngredienteQuantidade, ingredienteQuantidade);
    }

    public String getIngredienteDatacompra() {
        return ingredienteDatacompra;
    }

    public void setIngredienteDatacompra(String ingredienteDatacompra) {
        String oldIngredienteDatacompra = this.ingredienteDatacompra;
        this.ingredienteDatacompra = ingredienteDatacompra;
        changeSupport.firePropertyChange("ingredienteDatacompra", oldIngredienteDatacompra, ingredienteDatacompra);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ingredienteId != null ? ingredienteId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ingrediente)) {
            return false;
        }
        Ingrediente other = (Ingrediente) object;
        if ((this.ingredienteId == null && other.ingredienteId != null) || (this.ingredienteId != null && !this.ingredienteId.equals(other.ingredienteId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gui.Ingrediente[ ingredienteId=" + ingredienteId + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
