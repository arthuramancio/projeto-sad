/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Arthur
 */
@Entity
@Table(name = "produto", catalog = "dbsad", schema = "")
@NamedQueries({
    @NamedQuery(name = "Produto.findAll", query = "SELECT p FROM Produto p")
    , @NamedQuery(name = "Produto.findByProdutoIdproduto", query = "SELECT p FROM Produto p WHERE p.produtoIdproduto = :produtoIdproduto")
    , @NamedQuery(name = "Produto.findByProdutoNome", query = "SELECT p FROM Produto p WHERE p.produtoNome = :produtoNome")
    , @NamedQuery(name = "Produto.findByProdutoValor", query = "SELECT p FROM Produto p WHERE p.produtoValor = :produtoValor")
    , @NamedQuery(name = "Produto.findByProdutoFabricacao", query = "SELECT p FROM Produto p WHERE p.produtoFabricacao = :produtoFabricacao")
    , @NamedQuery(name = "Produto.findByProdutoValidade", query = "SELECT p FROM Produto p WHERE p.produtoValidade = :produtoValidade")})
public class Produto implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "produtoIdproduto")
    private Integer produtoIdproduto;
    @Basic(optional = false)
    @Column(name = "produtoNome")
    private String produtoNome;
    @Basic(optional = false)
    @Column(name = "produtoValor")
    private String produtoValor;
    @Basic(optional = false)
    @Column(name = "produtoFabricacao")
    private String produtoFabricacao;
    @Basic(optional = false)
    @Column(name = "produtoValidade")
    private String produtoValidade;

    public Produto() {
    }

    public Produto(Integer produtoIdproduto) {
        this.produtoIdproduto = produtoIdproduto;
    }

    public Produto(Integer produtoIdproduto, String produtoNome, String produtoValor, String produtoFabricacao, String produtoValidade) {
        this.produtoIdproduto = produtoIdproduto;
        this.produtoNome = produtoNome;
        this.produtoValor = produtoValor;
        this.produtoFabricacao = produtoFabricacao;
        this.produtoValidade = produtoValidade;
    }

    public Integer getProdutoIdproduto() {
        return produtoIdproduto;
    }

    public void setProdutoIdproduto(Integer produtoIdproduto) {
        Integer oldProdutoIdproduto = this.produtoIdproduto;
        this.produtoIdproduto = produtoIdproduto;
        changeSupport.firePropertyChange("produtoIdproduto", oldProdutoIdproduto, produtoIdproduto);
    }

    public String getProdutoNome() {
        return produtoNome;
    }

    public void setProdutoNome(String produtoNome) {
        String oldProdutoNome = this.produtoNome;
        this.produtoNome = produtoNome;
        changeSupport.firePropertyChange("produtoNome", oldProdutoNome, produtoNome);
    }

    public String getProdutoValor() {
        return produtoValor;
    }

    public void setProdutoValor(String produtoValor) {
        String oldProdutoValor = this.produtoValor;
        this.produtoValor = produtoValor;
        changeSupport.firePropertyChange("produtoValor", oldProdutoValor, produtoValor);
    }

    public String getProdutoFabricacao() {
        return produtoFabricacao;
    }

    public void setProdutoFabricacao(String produtoFabricacao) {
        String oldProdutoFabricacao = this.produtoFabricacao;
        this.produtoFabricacao = produtoFabricacao;
        changeSupport.firePropertyChange("produtoFabricacao", oldProdutoFabricacao, produtoFabricacao);
    }

    public String getProdutoValidade() {
        return produtoValidade;
    }

    public void setProdutoValidade(String produtoValidade) {
        String oldProdutoValidade = this.produtoValidade;
        this.produtoValidade = produtoValidade;
        changeSupport.firePropertyChange("produtoValidade", oldProdutoValidade, produtoValidade);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (produtoIdproduto != null ? produtoIdproduto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produto)) {
            return false;
        }
        Produto other = (Produto) object;
        if ((this.produtoIdproduto == null && other.produtoIdproduto != null) || (this.produtoIdproduto != null && !this.produtoIdproduto.equals(other.produtoIdproduto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gui.Produto[ produtoIdproduto=" + produtoIdproduto + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
