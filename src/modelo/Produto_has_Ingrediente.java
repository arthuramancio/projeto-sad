/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Arthur
 */
public class Produto_has_Ingrediente {
    int Produto_produtoIdproduto;
    int Ingrediente_idIngrediente;
    float Ingredientequantidade;

    public int getProduto_produtoIdproduto() {
        return Produto_produtoIdproduto;
    }

    public void setProduto_produtoIdproduto(int Produto_produtoIdproduto) {
        this.Produto_produtoIdproduto = Produto_produtoIdproduto;
    }

    public int getIngrediente_idIngrediente() {
        return Ingrediente_idIngrediente;
    }

    public void setIngrediente_idIngrediente(int Ingrediente_idIngrediente) {
        this.Ingrediente_idIngrediente = Ingrediente_idIngrediente;
    }

    public float getIngredientequantidade() {
        return Ingredientequantidade;
    }

    public void setIngredientequantidade(float Ingredientequantidade) {
        this.Ingredientequantidade = Ingredientequantidade;
    }


}