/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Arthur
 */
public class RegistroCompra_has_Produto {
    int RegistroCompra_registrocompraId;
    int Produto_idProduto;
    float Produtoquantidade;

    public float getProdutoquantidade() {
        return Produtoquantidade;
    }

    public void setProdutoquantidade(float Produtoquantidade) {
        this.Produtoquantidade = Produtoquantidade;
    }

    public int getRegistroCompra_registrocompraId() {
        return RegistroCompra_registrocompraId;
    }

    public void setRegistroCompra_registrocompraId(int RegistroCompra_registrocompraId) {
        this.RegistroCompra_registrocompraId = RegistroCompra_registrocompraId;
    }

    public int getProduto_idProduto() {
        return Produto_idProduto;
    }

    public void setProduto_idProduto(int Produto_idProduto) {
        this.Produto_idProduto = Produto_idProduto;
    }

    
}
