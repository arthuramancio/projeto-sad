/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Arthur
 */
public class Produto {
    int produtoIdproduto;
    String produtoNome;
    float produtoValor;
    String produtoFabricacao;
    String produtoValidade;
    int produtoQuantidade; 

    public int getProdutoQuantidade() {
        return produtoQuantidade;
    }

    public void setProdutoQuantidade(int produtoQuantidade) {
        this.produtoQuantidade = produtoQuantidade;
    }

    public int getProdutoIdproduto() {
        return produtoIdproduto;
    }

    public void setProdutoIdproduto(int produtoIdproduto) {
        this.produtoIdproduto = produtoIdproduto;
    }

    public String getProdutoNome() {
        return produtoNome;
    }

    public void setProdutoNome(String produtoNome) {
        this.produtoNome = produtoNome;
    }

    public float getProdutoValor() {
        return produtoValor;
    }

    public void setProdutoValor(float produtoValor) {
        this.produtoValor = produtoValor;
    }

    public String getProdutoFabricacao() {
        return produtoFabricacao;
    }

    public void setProdutoFabricacao(String produtoFabricacao) {
        this.produtoFabricacao = produtoFabricacao;
    }

    public String getProdutoValidade() {
        return produtoValidade;
    }

    public void setProdutoValidade(String produtoValidade) {
        this.produtoValidade = produtoValidade;
    }
    
}
