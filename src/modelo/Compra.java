/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Arthur
 */
public class Compra {
    int compraId;
    String compraFormaPagamento;
    //float compraValor;
    int Cliente_idCliente;    
    int RegistroCompra_registrocompraId;

    public int getCompraId() {
        return compraId;
    }

    public void setCompraId(int compraId) {
        this.compraId = compraId;
    }

    public String getCompraFormaPagamento() {
        return compraFormaPagamento;
    }

    public void setCompraFormaPagamento(String compraFormaPagamento) {
        this.compraFormaPagamento = compraFormaPagamento;
    }


    public int getCliente_idCliente() {
        return Cliente_idCliente;
    }

    public void setCliente_idCliente(int Cliente_idCliente) {
        this.Cliente_idCliente = Cliente_idCliente;
    }

    public int getRegistroCompra_registrocompraId() {
        return RegistroCompra_registrocompraId;
    }

    public void setRegistroCompra_registrocompraId(int RegistroCompra_registrocompraId) {
        this.RegistroCompra_registrocompraId = RegistroCompra_registrocompraId;
    }
    
    
   

    
}
