/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Arthur
 */
public class Ingrediente {
    int ingredienteId;
    String ingredienteNome;
    String ingredientePreco;
    String ingredienteQuantidade;
    String ingredienteDatacompra;
    String ingredienteMedida;

    public String getIngredienteMedida() {
        return ingredienteMedida;
    }

    public void setIngredienteMedida(String ingredienteMedida) {
        this.ingredienteMedida = ingredienteMedida;
    }

    public int getIngredienteId() {
        return ingredienteId;
    }

    public void setIngredienteId(int ingredienteId) {
        this.ingredienteId = ingredienteId;
    }

    public String getIngredienteNome() {
        return ingredienteNome;
    }

    public void setIngredienteNome(String ingredienteNome) {
        this.ingredienteNome = ingredienteNome;
    }

    public String getIngredientePreco() {
        return ingredientePreco;
    }

    public void setIngredientePreco(String ingredientePreco) {
        this.ingredientePreco = ingredientePreco;
    }

    public String getIngredienteQuantidade() {
        return ingredienteQuantidade;
    }

    public void setIngredienteQuantidade(String ingredienteQuantidade) {
        this.ingredienteQuantidade = ingredienteQuantidade;
    }

    public String getIngredienteDatacompra() {
        return ingredienteDatacompra;
    }

    public void setIngredienteDatacompra(String ingredienteDatacompra) {
        this.ingredienteDatacompra = ingredienteDatacompra;
    }

    
    
}
