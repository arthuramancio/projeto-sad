/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Arthur
 */
public class RegistroCompra {
    int registrocompraId;
    String registrocompraData;
    float registrocompraValor;
    int Usuario_usuarioId;

    public int getRegistrocompraId() {
        return registrocompraId;
    }

    public void setRegistrocompraId(int registrocompraId) {
        this.registrocompraId = registrocompraId;
    }

    public String getRegistrocompraData() {
        return registrocompraData;
    }

    public void setRegistrocompraData(String registrocompraData) {
        this.registrocompraData = registrocompraData;
    }

    public float getRegistrocompraValor() {
        return registrocompraValor;
    }

    public void setRegistrocompraValor(float registrocompraValor) {
        this.registrocompraValor = registrocompraValor;
    }

    public int getUsuario_usuarioId() {
        return Usuario_usuarioId;
    }

    public void setUsuario_usuarioId(int Usuario_usuarioId) {
        this.Usuario_usuarioId = Usuario_usuarioId;
    }

   
   
}
