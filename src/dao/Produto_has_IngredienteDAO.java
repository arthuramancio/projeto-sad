/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import factory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import modelo.Produto_has_Ingrediente;

/**
 *
 * @author Arthur
 */
public class Produto_has_IngredienteDAO {
    private Connection connection;
    int Produto_produtoIdproduto;
    int Ingrediente_idIngrediente;
    float Ingredientequantidade;
    public Produto_has_IngredienteDAO(){
        this.connection = new ConnectionFactory().getConnection();
    }
    public void adiciona(Produto_has_Ingrediente produto_has_ingrediente) throws SQLException{
        String sql = "INSERT INTO Produto_has_Ingrediente(Produto_produtoIdproduto, Ingrediente_idIngrediente, Ingredientequantidade) VALUES(?, ?, ?)";
        
        
        try{
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {
                stmt.setInt(1, produto_has_ingrediente.getProduto_produtoIdproduto());
                stmt.setInt(2, produto_has_ingrediente.getIngrediente_idIngrediente());
                stmt.setFloat(3, produto_has_ingrediente.getIngredientequantidade());                
                stmt.execute();
                stmt.close();
            }
        }
        catch (SQLException u) { 
            throw new RuntimeException(u);
        }
    }
}
