/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import factory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import modelo.Compra;

/**
 *
 * @author Arthur
 */
public class CompraDAO {
    private Connection connection;
    String compraFormaPagamento;
    //float compraValor;
    int Cliente_idCliente;    
    int RegistroCompra_registrocompraId;
    
    public CompraDAO(){
        this.connection = new ConnectionFactory().getConnection();
    }
    public void adiciona(Compra compra) throws SQLException{
        String sql = "INSERT INTO Compra(compraFormaPagamento, Cliente_idCliente, RegistroCompra_registrocompraId) VALUES( ?, ?, ?)";
        try{
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {
                stmt.setString(1, compra.getCompraFormaPagamento());
                stmt.setInt(2, compra.getCliente_idCliente());  
                stmt.setInt(3 ,compra.getRegistroCompra_registrocompraId());
                stmt.execute();
                stmt.close();
            }
        }
        catch (SQLException u) { 
            throw new RuntimeException(u);
        }
    }

}
