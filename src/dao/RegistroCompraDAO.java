/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import factory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import modelo.RegistroCompra;

/**
 *
 * @author Eduardo
 */
public class RegistroCompraDAO {
    
    private Connection connection;
    
    String registrocompraData;
    float registrocompraValor;
    int Usuario_usuarioId;

    
   
    
    public RegistroCompraDAO(){
        this.connection = new ConnectionFactory().getConnection();
    }
    public void adiciona(RegistroCompra registroCompra) throws SQLException{
        String sql = "INSERT INTO RegistroCompra (registrocompraData,registrocompraValor, Usuario_usuarioId ) VALUES(?, ?, ?)";
        try{
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {
                stmt.setString(1, registroCompra.getRegistrocompraData()); 
                stmt.setFloat(2, registroCompra.getRegistrocompraValor());
                stmt.setInt(3, registroCompra.getUsuario_usuarioId());
                stmt.execute();
                stmt.close();
            }
        }
        catch (SQLException u) { 
            throw new RuntimeException(u);
        }
    }
    
}
