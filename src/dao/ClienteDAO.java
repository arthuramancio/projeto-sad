/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import factory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modelo.Cliente;

/**
 *
 * @author Arthur
 */
public class ClienteDAO {
    private Connection connection;
    int clienteId;
    String clienteNome;
    String clienteCpf;
    String clienteEndereco;
    String clienteTelefone;
    String testeeeee;
    
    public ClienteDAO(){
        this.connection = new ConnectionFactory().getConnection();
    }
    public void adiciona(Cliente cliente) throws SQLException{
        String sql = "INSERT INTO Cliente(clienteNome, clienteCpf, clienteEndereco, clienteTelefone) VALUES(?, ?, ?, ?)";
        try{
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {
                stmt.setString(1, cliente.getClienteNome());
                stmt.setString(2, cliente.getClienteCpf());
                stmt.setString(3, cliente.getClienteEndereco());
                stmt.setString(4, cliente.getClienteTelefone());               
                stmt.execute();
                stmt.close();
            }
        }
        catch (SQLException u) { 
            throw new RuntimeException(u);
        }
    }
    
    /*public ArrayList<Cliente> getList() throws Exception{
        String sql = ("select * from cliente");
        try{
           PreparedStatement stmt = connection.prepareStatement(sql);         
           ResultSet rs = stmt.executeQuery(sql);
           ArrayList<Cliente> lista = new ArrayList<Cliente>();
           Cliente pesDoc;
            while(rs.next()){
                pesDoc = new Cliente(rs.getString(clienteNome),Integer.parseInt(rs.getString(clienteId)));
                lista.add(pesDoc);
            }
        }catch (SQLException u) { 
            throw new RuntimeException(u);
        }
        
        rs.close(); 
        return lista;
    }
    */

}
