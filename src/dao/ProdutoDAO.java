/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import factory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import modelo.Produto;

/**
 *
 * @author Arthur
 */
public class ProdutoDAO {
    private Connection connection;
    int produtoIdproduto;
    String produtoNome;
    float produtoValor;
    String produtoFabricacao;
    String produtoValidade;
    int produtoQuantidade; 

    
    public ProdutoDAO(){
        this.connection = new ConnectionFactory().getConnection();
    }
    public void adiciona(Produto produto) throws SQLException{
        String sql = "INSERT INTO Produto(produtoidProduto, produtoNome, produtoValor, produtoFabricacao, produtoValidade,produtoQuantidade) VALUES(?,?, ?, ?, ?, ?)";
        try{
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {
                stmt.setInt(1, produto.getProdutoIdproduto());
                stmt.setString(2, produto.getProdutoNome());
                stmt.setFloat(3, produto.getProdutoValor());
                stmt.setString(4, produto.getProdutoFabricacao());
                stmt.setString(5, produto.getProdutoValidade());
                stmt.setInt(6, produto.getProdutoQuantidade());                
                stmt.execute();
                stmt.close();
            }
        }
        catch (SQLException u) { 
            throw new RuntimeException(u);
        }
    }
}
