/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import factory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import modelo.RegistroCompra_has_Produto;

/**
 *
 * @author Arthur
 */
public class RegistroCompra_has_ProdutoDAO {
    private Connection connection;
    int RegistroCompra_registrocompraId ;
    int Produto_idProduto;
    float Produtoquantidade;
   
    
    public RegistroCompra_has_ProdutoDAO(){
        this.connection = new ConnectionFactory().getConnection();
    }
    public void adiciona(RegistroCompra_has_Produto registroCompra_has_produto) throws SQLException{
        String sql = "INSERT INTO RegistroCompra_has_Produto(RegistroCompra_registrocompraId, Produto_idProduto, Produtoquantidade) VALUES(?, ?, ?)";
        try{
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {
                stmt.setInt(1, registroCompra_has_produto.getRegistroCompra_registrocompraId());
                stmt.setInt(2, registroCompra_has_produto.getProduto_idProduto());
                stmt.setFloat(3, registroCompra_has_produto.getProdutoquantidade());
                stmt.execute();
                stmt.close();
            }
        }
        catch (SQLException u) { 
            throw new RuntimeException(u);
        }
    }
}
