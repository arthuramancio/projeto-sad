/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import factory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import modelo.Ingrediente;

/**
 *
 * @author Arthur
 */
public class IngredienteDAO {
    private Connection connection;
    String ingredienteNome;
    String ingredientePreco;
    String ingredienteQuantidade;
    String ingredienteDatacompra;
    String ingredienteMedida;
    public IngredienteDAO(){
        this.connection = new ConnectionFactory().getConnection();
    }
    public void adiciona(Ingrediente ingrediente) throws SQLException{
        String sql = "INSERT INTO Ingrediente(ingredienteNome, ingredientePreco, ingredienteQuantidade, ingredienteDatacompra,ingredienteMedida) VALUES(?,?, ?, ?, ?)";
        try{
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {
                stmt.setString(1, ingrediente.getIngredienteNome());
                stmt.setString(2, ingrediente.getIngredientePreco());
                stmt.setString(3, ingrediente.getIngredienteQuantidade());
                stmt.setString(4, ingrediente.getIngredienteDatacompra());
                stmt.setString(5, ingrediente.getIngredienteMedida());
                stmt.execute();
                stmt.close();
            }
        }
        catch (SQLException u) { 
            throw new RuntimeException(u);
        }
    }

}
