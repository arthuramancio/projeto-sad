/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;
import factory.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Arthur
 */
public class UsuarioDAO {
    private Connection connection;
    String usuarioLogin;
    String usuarioSenha;
    public UsuarioDAO(){
        this.connection = new ConnectionFactory().getConnection();
    }
    public boolean checkLogin(String login, String senha) {
        String sql = "SELECT * FROM usuario WHERE usuarioLogin = ? and usuarioSenha = ?";
        ResultSet rs = null;
        boolean check = false;
        try{
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {
                stmt.setString(1, usuarioLogin);
                stmt.setString(2, usuarioSenha);            
                stmt.execute();
                rs = stmt.executeQuery();
                if (rs.next()) {               
                    check = true;
                }
                stmt.close();
            }
        }
        catch (SQLException u) { 
            throw new RuntimeException(u);
        }
        return check;
    }
}
